
async function fetchData() {
	let result = await fetch("https://jsonplaceholder.typicode.com/todos")
	let json = await result.json()
	console.log(json)
}
fetchData();